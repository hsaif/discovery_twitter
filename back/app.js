var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var searchRouter = require("./routes/search")

var app = express();
const mongoose = require("mongoose");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const cors = require("cors");
app.use(cors());

app.get(
  "/",
  (req, res) => res.send("Twitter Collections Discovery Portal") // Home web page
);

const CollectionSchema = require("./model");
const router = express.Router();

app.use("/db", router);
router.route("/find").get(async (req, res) => {
  const response = await CollectionSchema.find();
  return res.status(200).json(response);
});


app.get("/db/find/:eventId", async (req, res) => {
  const { eventId } = req.params;
  console.log('event id:', eventId);
  try {
    const event = await CollectionSchema.findById(eventId); 
    if (!event) {
      return res.status(404).send("Event not found");
    }
    res.json(event);
  } catch (error) {
    res.status(500).send("Server error");
  }
});

app.use("/db/search", searchRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

// Connect to MongoDB database
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://127.0.0.1:27017/twitterCollections");
mongoose.connection.once("open", function () {
  console.log("Connection with MongoDB was successful");
});

module.exports = app;
