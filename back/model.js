// Import mongoose library
const mongoose = require("mongoose");

// Define schema
const CollectionSchema = new mongoose.Schema({
  db: {},
  id: {},
  source: {},
  terms: {},
  wiki: {},
  description: {},
  tags: {},
  cat1: {},
  cat2: {},
  cat3: {},
  name: {},
  date: {},
  numTweets: {},
});

// Export the schema
module.exports = mongoose.model("Collection", CollectionSchema, "tweets");