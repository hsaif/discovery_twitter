var express = require("express");
var router = express.Router();
var CollectionSchema = require("../model");

// GET keyword search
router.get("/", async (req, res) => {
  const { query } = req.query;
  try {
    // partial case-insensitive search for matching name/terms
    const results = await CollectionSchema.find({
      $or: [
        { name: { $regex: query, $options: "i" }},
        { terms: { $regex: query, $options: "i" }}
      ],
    });
    res.status(200).json(results);
  } catch (error) {
    res.status(500).send("Server error");
  }
});

module.exports = router;
