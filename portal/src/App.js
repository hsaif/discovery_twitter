import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Event from './pages/Event';

// entry point of the app
// responsible for dynamic page routing of the Home/Event pages
function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/event/:eventId" element={<Event />} />
      </Routes>
    </Router>
  );
}

export default App;