// BackButton.js
import React from 'react';
import Button from '@mui/material/Button';

const BackButton = () => {
    const handleClick = () => {
        window.location.href = '/home';
    };

    return (
        <Button variant="contained" color="primary" onClick={handleClick}>
            Back
        </Button>
    );
};

export default BackButton;