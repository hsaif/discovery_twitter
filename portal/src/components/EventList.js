import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { List, ListItem, ListItemText, Grid, Pagination, Button, TextField, Paper, MenuItem, Select, Typography } from "@mui/material";
import axios from "axios";

function EventList({ filters }) {
  
  const {
    alphabetical,
    chronological,
    fromDate,
    toDate,
    minTweets,
    maxTweets,
    source
  } = filters;

  const [events, setEvents] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const [searchQuery, setSearchQuery] = useState("");  
  
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(50);

  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = (page * itemsPerPage);

  useEffect(() => {
    const fetchEvents = async () => {
      try {
        let url = "http://localhost:3000/db/find";
        if (searchQuery) {
            url = `http://localhost:3000/db/search?query=${encodeURIComponent(searchQuery)}`;
        }
        const response = await axios.get(url);
        setEvents(response.data);
      } catch (error) {
        console.error("Error fetching events:", error);
      }
    };

    fetchEvents();
  }, [searchQuery]);

  const handleSearchChange = (event) => {
    setSearchInput(event.target.value);
  };

  const handleSearchClick = () => {
    setSearchQuery(searchInput);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };
  
  const handleItemsPerPageChange = (event) => {
    const items = event.target.value;
    setItemsPerPage(items);
    setPage(1);
    setTotalPages(Math.ceil(filteredEvents.length / items));
  };

  const filteredEvents = events.filter((event) => {
    return (
      (!source || source === 'All' || event.source.includes(source)) &&
      (!fromDate || event.date >= fromDate) &&
      (!toDate || event.date <= toDate) &&
      (!minTweets || event.numTweets >= minTweets) &&
      (!maxTweets || event.numTweets <= maxTweets)
    );
  });

  const sortedEvents = [...filteredEvents].sort((a, b) =>
    alphabetical && chronological
      ? a.name.localeCompare(b.name) || new Date(a.date) - new Date(b.date)
      : alphabetical
      ? a.name.localeCompare(b.name)
      : chronological
      ? new Date(a.date) - new Date(b.date)
      : 0
  );

  // update pagination when filters change
  useEffect(() => {
    const newTotalPages = Math.ceil(sortedEvents.length / itemsPerPage);
    if (page > newTotalPages) {
        setPage(1);
    }
    setTotalPages(newTotalPages);
  }, [sortedEvents, itemsPerPage, page]);


  return (
    <div>
    {/* Total number of collections/tweets */}
    <Grid container spacing={2} justifyContent="space-between">
      <Grid item>
        <Grid container alignItems="center">
          <Grid item>
          {events.length !== 0 ? (
          <Typography variant="h6" style={{marginLeft:"16px"}}>
            Collections: {sortedEvents.length} | Tweets:{" "}
            {sortedEvents.reduce((total, event) => total + (event.numTweets || 0), 0).toLocaleString()}
          </Typography>
            ) : (
            <Typography variant="h6" style={{marginLeft:"16px"}}>
                Loading...
            </Typography>
            )}      
          </Grid>
        </Grid>
      </Grid>

      {/* Search bar */}
      <Grid item>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <TextField
                label="Search by name/term..."
                size="small"
                variant="outlined"
                sx={{ minWidth: 300 }}
                value={searchInput}
                onChange={handleSearchChange}
            />          
        </Grid>
          <Grid item marginRight="16px">
            <Button 
                variant="contained" 
                onClick={handleSearchClick}>Search</Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
        
      {/* The event list */}
      <Paper elevation={4}>
        <List style={{ margin: "8px", maxHeight: "450px", overflowY: "auto" }}>
          {sortedEvents.slice(startIndex, endIndex)
              .map((event) => (
            <ListItem
              button
              key={event._id}
              component={Link}
              to={`/event/${encodeURIComponent(event._id)}`}>
              <ListItemText
                primary={event.name}
                secondary={`id: ${event.id} | ${new Date(
                  event.date
                ).toLocaleDateString()} | tweets: ${event.numTweets.toLocaleString()}`}
              />
            </ListItem>
          ))}
        </List>
      </Paper>
    
      {/* Pagination of the list */}
      <Grid container spacing={3} alignItems="center">
        <Grid item>
          <Typography variant="subtitle1">Rows per page:</Typography>
        </Grid>
        <Grid item>
          <Select
            value={itemsPerPage}
            onChange={handleItemsPerPageChange}
            variant="standard"
            displayEmpty
            sx={{ minWidth: 60 }} >
            <MenuItem value={25}>25</MenuItem>
            <MenuItem value={50}>50</MenuItem>
            <MenuItem value={100}>100</MenuItem>
          </Select>
        </Grid>
        <Grid item>
          <Pagination
            count={totalPages}
            page={page}
            onChange={handlePageChange}
            siblingCount={2}
            color="primary"
            size="large"
            variant="outlined"
            shape="rounded" />
        </Grid>
      </Grid>
    </div>
  );
}

export default EventList;
