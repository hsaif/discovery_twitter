import React from 'react';
import logo from '../images/vt-logo.png';
import { Typography } from '@mui/material';

// Header displays the name and VT logo on every page
function Header() {
  return (
    <div style={{ display: 'flex', alignItems: 'center', margin: '8px'}}>

      {/* logo */}
      <img src={logo} alt="Logo" style={{ maxHeight: '100px', verticalAlign: 'middle' }} />

      {/* header */}
      <Typography variant="h3" component="h1" style={{ padding: '8px', verticalAlign: 'middle' }}>
        Twitter Event Collections
      </Typography>
    </div>
  );
}

export default Header;