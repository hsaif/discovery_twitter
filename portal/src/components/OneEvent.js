// OneEvent.js
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Card, CardHeader, CardContent, Typography } from "@mui/material";
import axios from "axios";
import BackButton from './BackButton';

function OneEvent() {
  const { eventId } = useParams();
  const [event, setEvent] = useState(null);

  useEffect(() => {
    const fetchEvent = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3000/db/find/${eventId}` // Send
        );
        setEvent(response.data); // Assume the backend returns a single event object
      } catch (error) {
        console.error("Error fetching event:", error);
      }
    };

    fetchEvent();
  }, [eventId]);

  if (!event) {
    return <Typography>Loading...</Typography>; // Display loading state before data arrives
  }

  return (
    <Card>
      <CardHeader title={event.name} subheader={"collection id: " + event.id } />
      <CardContent>
            <Typography variant="body1"> 
            START DATE:{" " + new Date(event.date).toLocaleDateString()}
            </Typography>
            <Typography variant="body1">
            NUMBER OF TWEETS:{" " + event.numTweets.toLocaleString()}
            </Typography>
            <Typography variant="body1">DATABASE: {" " + event.db}</Typography>
            <Typography variant="body1">SOURCE: {" " + event.source}</Typography>
            <Typography variant="body1">WIKIPEDIA: {" " + event.wiki}</Typography>
            <Typography variant="body1">TAGS: {" " + event.tags}</Typography>
            <Typography variant="body1">TERMS: {" " + event.terms}</Typography>
            <Typography variant="body1">CATEGORIES: {" " + event.cat1} {" " + event.cat2} {" " + event.cat3}
            </Typography>
            <Typography variant="body1">DESCRIPTION: {" " + event.description}</Typography>
            <BackButton />
      </CardContent>
    </Card>
  );
}

export default OneEvent;