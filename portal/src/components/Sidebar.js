import React, { useState, useEffect } from 'react';
import { FormGroup, InputLabel, Box, Select, TextField, FormControl, MenuItem, Button, FormControlLabel, Checkbox } from '@mui/material';
import axios from 'axios';

  // Sidebar manages the filtering/sorting options
  function Sidebar({ onFilterChange }) {

  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [minTweets, setMinTweets] = useState("");
  const [maxTweets, setMaxTweets] = useState("");
  const [source, setSource] = useState("");
  const [allSources, setAllSources] = useState([]);

  // Fetch the set of collection sources from server
  useEffect(() => {
    const fetchSources = async () => {
      try {
        const response = await axios.get("http://localhost:3000/db/find");
        const data = [...new Set(response.data
            .flatMap(event => event.source.split(',')
            .map(s => s.trim())).filter(s => s !== ''))].sort();
        setAllSources(data);
        setSource("All");   // show all by default
      } catch (error) {
        console.error('Error fetching sources:', error);
      }
    };
    if (allSources.length === 0) {
      fetchSources();
    }
  }, [allSources]);

  // handle changes for sorting
  const handleCheckboxChange = (event) => {
    const { name, checked } = event.target;
    onFilterChange(name, checked);
  };

  // handle changes for filtering
  const handleFromDateChange = (event) => {
    setFromDate(event.target.value);
    onFilterChange('fromDate', event.target.value);
  };

  const handleToDateChange = (event) => {
    setToDate(event.target.value);
    onFilterChange('toDate', event.target.value);
  };

  const handleMinTweetsChange = (event) => {
    setMinTweets(event.target.value);
    onFilterChange('minTweets', event.target.value);
  }

  const handleMaxTweetsChange = (event) => {
    setMaxTweets(event.target.value);
    onFilterChange('maxTweets', event.target.value);
  }

  const handleSourceChange = (event) => {
    setSource(event.target.value);
    onFilterChange('source', event.target.value);
  };

  // clear all the filters
  const clearAllFilters = () => {
    setFromDate('');
    setToDate('');
    setMinTweets('');
    setMaxTweets('');
    setSource('All');
    onFilterChange('fromDate', '');
    onFilterChange('toDate', '');
    onFilterChange('minTweets', '');
    onFilterChange('maxTweets', '');
    onFilterChange('source', 'All');
  }

  return (
    <div>
      {/* Checkboxes for sorting */}
      <FormGroup>
        <FormControlLabel
          control={<Checkbox name="alphabetical" onChange={handleCheckboxChange} />}
          label="Alphabetical"
        />
        <FormControlLabel
          control={<Checkbox name="chronological" onChange={handleCheckboxChange} />}
          label="Chronological"
        />
      </FormGroup>
      
      {/* spacing */}
      <Box mt={2} />

      {/* 'From' Date chooser */}
      <FormGroup row style={{ display: 'flex' }}>
        <TextField
          id="from-date"
          label="From"
          type="date"
          value={fromDate}
          onChange={handleFromDateChange}
          InputLabelProps={{
            shrink: true,
          }}
          style={{ flexGrow: 1 }}
        />

      {/* spacing */}
      <Box mr={2} />

      {/* 'To' Date chooser */}
      <TextField
        id="to-date"
        label="To"
        type="date"
        value={toDate}
        onChange={handleToDateChange}
        InputLabelProps={{
          shrink: true,
        }}
        style={{ flexGrow: 1 }}
      />
      </FormGroup>

      {/* Spacing */}
      <Box mt={2}/>

      {/* Number of tweets range */}
      <InputLabel
        style={{ marginBottom: "16px", marginLeft: "8px" }}>Number of Tweets</InputLabel>

      <FormGroup row style={{ display: 'flex' }}>
        <TextField
          id="min-tweets"
          label="Min"
          type="number"
          value={minTweets}
          onChange={handleMinTweetsChange}
          InputLabelProps={{
            shrink: true,
          }}
          style={{ flexGrow: 1 }}
        />

      {/* spacing */}
      <Box mr={2}/>

      <TextField
        id="max-tweets"
        label="Max"
        type="number"
        value={maxTweets}
        onChange={handleMaxTweetsChange}
        InputLabelProps={{
          shrink: true,
        }}
        style={{ flexGrow: 1 }}
      />
      </FormGroup>
      
      {/* spacing */}
      <Box mt={2} />

      {/* Select source */}
      <InputLabel
        style={{ marginBottom: "8px", marginLeft: "8px" }}>Source</InputLabel>
      <FormControl fullWidth>
        <Select
          value={source}
          onChange={handleSourceChange}
        >
          <MenuItem value="All">All</MenuItem>
          {allSources.map(s => (
            <MenuItem key={s} value={s}>{s}</MenuItem>
          ))}
        </Select>
      </FormControl>

      {/* Clear all button */}
      <FormGroup row>
        <Button variant="text" onClick={clearAllFilters}>Clear All</Button>
      </FormGroup>
    </div>
  );
}

export default Sidebar;
