import React from 'react';
import { useParams } from 'react-router-dom';
import { Typography } from '@mui/material'
// import { Grid } from '@mui/material';
import Header from '../components/Header';
import OneEvent from '../components/OneEvent';

// Event page will display data about the event that was fetched
function Event() {

  const { eventID } = useParams();

  return (
    <div>
      <Header/>
      <Typography style= {{margin:'16px'}} variant="h5">
        <OneEvent filter={eventID}></OneEvent>
      </Typography>
    </div>
  );
}

export default Event;