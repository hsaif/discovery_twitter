import { React, useState } from 'react';
import { Grid } from '@mui/material';
import EventList from '../components/EventList';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';


// Home page handles the Sidebar and EventList
function Home() {

  // filter criteria for event list
  const [filters, setFilters] = useState({
    alphabetical: false,
    chronological: false,
    fromDate: null,
    toDate: null,
    minTweets: null,
    maxTweets: null,
    source: null
  });

  // handles changes to the filter selection
  const handleFilterChange = (filterName, value) => {
    setFilters((prevFilters) => ({
      ...prevFilters,
      [filterName]: value,
    }));
  };

  return (
    <div>
      {/* Header with title and the logo */}
      <Header/>

      {/* Grid used to organize the Sidebar and EventList */}
      <Grid container spacing={5} sx={{ padding: '20px' }}>
        <Grid item xs={4}>
          <Sidebar onFilterChange={handleFilterChange} />
        </Grid>

        <Grid item xs={8}>
          <EventList filters={filters}/>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;